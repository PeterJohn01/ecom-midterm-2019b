-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 18, 2019 at 10:34 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db`
--

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `contact` int(11) NOT NULL,
  `email` text NOT NULL,
  `address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`id`, `name`, `contact`, `email`, `address`) VALUES
(929, 'john', 988, 'gasjas@gmail', 'abscabscman'),
(91929, 'jackson', 2147483647, 'hagsj', 'jkashcj'),
(123199, 'jdshkshksjdkj', 912389128, 'kjdvksldvbkj', 'jhkdjhskdfk'),
(361827, 'jhdslfsdksjh', 918239129, '.cnv,mx.nc', '.kdjfvnkdjfk'),
(535256, 'jb,bx,cmb,xmb', 72837648, 'bdv,bj', ',mbldkfbvkj'),
(738712, 'kjdlskj', 9291238, 'j.lkvdsjd', 'klkvsdks'),
(929291, 'jksdfhkjs', 939393, 'jdhkjs', 'kjsdfkjs'),
(1232131, 'kjllkjsdhkf', 9399929, 'vbnn,ckvj', 'dfkjvdk'),
(5127277, 'kjhskjldj', 78163162, 'jb,sdbj', 'hjbsjdbj'),
(73681268, 'lfdhbjdbfj', 1873981, '.kjvjbsdjvbsk', 'kjdjvkksdj'),
(82342378, 'vjkdhfkvhdkj', 2147483647, 'kjbkjsbdkjsskj', 'kjhkdhkskdj'),
(931212312, 'dhgfjsd', 9128031, 'jghdfjs', 'kjsdfjs');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
