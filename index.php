<?php
   require "add.php";
?>

<!DOCTYPE html>
<html>
 <link rel="stylesheet" type="text/css" href="style.css">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 
<head>
	<title>Calling Card Information</title>
</head>

<body>
    
     <nav>
       <form action="add.php" method="POST"> 
          
          <div class="input-group">
			<label>ID</label>
			<input type="text" name="id">
		  </div>
       	  <div class="input-group">
			<label>Name</label>
			<input type="text" name="name">
		   </div>
		   <div class="input-group">
			<label>Contact</label>
			<input type="text" name="contact">
		   </div>
		   <div class="input-group">
			<label>Email</label>
			<input type="text" name="email">
		   </div>
		   <div class="input-group">
			<label>Address</label>
			<input type="text" name="address">
		   </div>
		   <div class="input-group">
		      <br>
			 <button type ="submit" name="save" value="Save" class="add_btn">Add</button>
		   </div>

       </form>
       
     	
     </nav>
      
    <section>
    	
    	<table>
      
    	<tr>
    	    <th>ID</th>
    		<th>Name</th>
    		<th>Contact</th>
    		<th>Email</th>
    		<th>Address</th>
    		<th colspan="2">Action</th>
    	</tr>

    	<?php 
		
		
					   while ($row = mysqli_fetch_array($res)) { 
          	                    echo "<tr>";
          	                    echo "<td>".$row['id']."</td>";
          	                	echo "<td>".$row['name']."</td>";
		                    	echo "<td>".$row['contact']."</td>";
		                    	echo "<td>".$row['email']."</td>";
		                    	echo "<td>".$row['address']."</td>";
          	                	echo "<td> <a href=\"edit.php?id=$row[id]\"> Edit </a> </td>";
          	                	echo "<td> <a href=\"delete.php?id=$row[id]\" onClick=\"return confirm('Are you sure you want to delete?')\"> Delete </a></td>";
	                	}
	             ?>

    </table>
	<div class="center">
	      <ul class="pagination">
        <li><a href="?pageno=1">First</a></li>
        <li class="<?php if($pageno <= 1){ echo 'disabled'; } ?>">
            <a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>">Prev</a>
        </li>
        <li class="<?php if($pageno >= $total_pages){ echo 'disabled'; } ?>">
            <a href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>">Next</a>
        </li>
        <li><a href="?pageno=<?php echo $total_pages; ?>">Last</a></li>
    </ul>
	</div>
	

    </section>
    
</body>
</html>